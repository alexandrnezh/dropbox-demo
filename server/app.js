const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { PORT } = require('./src/config/config');
const fileRouter = require('./src/routes/file.route');
const authRouter = require('./src/routes/auth.route');
const logger = require('./src/helpers/logger.helper');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use('/files', fileRouter);
app.use('/auth', authRouter);

app.listen(PORT, () => {
  logger.log('info', `Server has started on port ${PORT}`);
});
