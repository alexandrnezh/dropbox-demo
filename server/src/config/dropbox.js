const { Dropbox } = require('dropbox');
const fetch = require('node-fetch');
const { DROPBOX_ACCESS_TOKEN } = require('./config');

const options = {
  accessToken: DROPBOX_ACCESS_TOKEN,
  fetch,
};
const dbx = new Dropbox(options);
module.exports = dbx;
