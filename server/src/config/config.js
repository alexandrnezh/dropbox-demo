const dotenv = require('dotenv');

const result = dotenv.config({ path: 'D:/Projects/dropbox-demo/server/.env' });

if (result.error) {
  throw result.error;
}

const { parsed: envs } = result;
module.exports = envs;
