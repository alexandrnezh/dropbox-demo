const File = require('../../models/file.model');
const logger = require('../../helpers/logger.helper');
const { getQuery } = require('../../helpers/query.helper');

exports.getAll = async (queryParams) => {
  const dbQuery = getQuery(queryParams);
  try {
    logger.log(dbQuery);
    const res = await File.find(dbQuery).populate('parentFolder').populate('files');
    logger.log('info', `File service: getAll > ${JSON.stringify(res)}`);
    return res;
  } catch (err) {
    logger.log('error', `File service: getAll > ${err}`);
  }
  return null;
};

exports.get = async (id) => {
  try {
    const res = await File.findById(id).populate('parentFolder');
    logger.log('info', `File service: get > ${JSON.stringify(res)}`);
    return res;
  } catch (err) {
    logger.log('error', `File service: get > ${err}`);
  }
  return null;
};

exports.create = async (fileData) => {
  fileData.isUpdated = false;
  fileData.dateUpdated = null;
  fileData.dateCreated = new Date();
  try {
    const res = await File.create(fileData);
    if (res.parentFolder) {
      const parentFolder = await this.get(res.parentFolder);
      parentFolder.files.push(res._id);
      parentFolder.save();
    }
    logger.log('info', `File service: create > ${JSON.stringify(res)}`);
    return res;
  } catch (err) {
    logger.log('error', `File service: create > ${err}`);
  }
  return null;
};

exports.delete = async (id) => {
  try {
    const res = await File.findByIdAndDelete(id);
    logger.log('info', `File service: delete > ${JSON.stringify(res)}`);
    return res;
  } catch (err) {
    logger.log('error', `File service: delete > ${err}`);
  }
  return null;
};

exports.update = async (id, fileData) => {
  fileData.isUpdated = true;
  fileData.dateUpdated = new Date();
  try {
    const res = await File.findByIdAndUpdate(id, fileData);
    logger.log('info', `File service: update > ${JSON.stringify(res)}`);
    return res;
  } catch (err) {
    logger.log('error', `File service: update > ${err}`);
  }
  return null;
};
