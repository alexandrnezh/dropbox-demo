const mongoose = require('../db/connection');

const { Schema } = mongoose;

const fileSchema = new Schema(
  {
    name: { type: mongoose.SchemaTypes.String, required: true },
    path: { type: mongoose.SchemaTypes.String, required: true },
    size: { type: mongoose.SchemaTypes.Number, required: false },
    isUpdated: { type: mongoose.SchemaTypes.Boolean, required: true },
    isFolder: { type: mongoose.SchemaTypes.Boolean, required: true },
    dateUpdated: { type: mongoose.SchemaTypes.Date, required: false },
    dateCreated: { type: mongoose.SchemaTypes.Date, required: true },
    type: { type: mongoose.SchemaTypes.String, required: true },
    extension: { type: mongoose.SchemaTypes.String, required: false },
    files: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'file' }],
    parentFolder: { type: mongoose.SchemaTypes.ObjectId, ref: 'file' },
  },
  { versionKey: false },
);

const collectionName = 'file';
const File = mongoose.model(collectionName, fileSchema);
module.exports = File;
