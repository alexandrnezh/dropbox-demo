const router = require('express').Router();
const parser = require('body-parser').urlencoded({ extended: true });
const { verifyToken } = require('../middlewares/auth.middleware');

const fileController = require('../controllers/file.controller');

router.get('/', verifyToken, fileController.getAll);
router.get('/:id', verifyToken, fileController.get);
router.post('/createFolder', parser, verifyToken, fileController.createFolder);
router.post('/createFile', parser, verifyToken, fileController.createFile);
router.put('/:id', parser, verifyToken, fileController.update);
router.delete('/:id', verifyToken, fileController.delete);

module.exports = router;
