exports.getQuery = (queryParams) => {
  const query = {};
  Object.entries(queryParams).forEach(([key, value]) => {
    if (value !== undefined) {
      if (key === 'path' && (value === '/' || value === '')) {
        query.parentFolder = null;
      } else query[key] = value;
    }
  });
  return query;
};
