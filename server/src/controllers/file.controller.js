const fileService = require('../db/services/file.service');
const dbx = require('../config/dropbox');
const logger = require('../helpers/logger.helper');

exports.getAll = async (req, resp) => {
  const queryParams = req.query;
  logger.log('info', 'File controller: getAll >');

  const res = await fileService.getAll(queryParams);
  if (!res) return resp.status(404).send({ code: 404, message: 'Not Found' });
  return resp.status(200).send(res);
};

exports.get = async (req, resp) => {
  const { id } = req.params;
  logger.log('info', 'File controller: get >');

  const res = await fileService.get(id);
  if (!res) return resp.status(404).send({ code: 404, message: 'Not Found' });
  return resp.status(200).send(res);
};

exports.createFolder = async (req, resp) => {
  logger.log('info', 'File controller: create >');
  dbx
    .filesCreateFolderV2({
      path: req.body.path,
      autorename: true,
    })
    .then(async (data) => {
      req.body.name = data.metadata.name;
      req.body.path = data.metadata.path_display;
      const res = await fileService.create(req.body);
      if (!res) return resp.status(400).send({ code: 400, message: 'Bad Request' });
      return resp.status(201).send(res);
    })
    .catch((err) => {
      logger.log(err);
      resp.status(400).send({ code: 400, message: 'Bad Request' });
    });
};

exports.createFile = async (req, resp) => {
  logger.log('info', 'File controller: create >');
  const res = await fileService.create(req.body);
  if (!res) return resp.status(400).send({ code: 400, message: 'Bad Request' });
  return resp.status(201).send(res);
};

exports.delete = async (req, resp) => {
  const { id } = req.params;
  logger.log('info', 'File controller: delete >');
  const file = await fileService.get(id);
  dbx
    .filesDeleteV2({
      path: file.path,
    })
    .then(async (data) => {
      const res = await fileService.delete(id);
      if (!res) return resp.status(400).send({ code: 400, message: 'Bad Request' });
      return resp.status(201).send(res);
    })
    .catch((err) => {
      logger.log(err);
      resp.status(400).send({ code: 400, message: 'Bad Request' });
    });
  const res = await fileService.delete(id);
  if (!res) return resp.status(400).send({ code: 400, message: 'Bad Request' });
  return resp.status(204).send();
};

exports.update = async (req, resp) => {
  const { id } = req.params;
  logger.log('info', 'File controller: update >');

  const res = await fileService.update(id, req.body);
  if (!res) return resp.status(400).send({ code: 400, message: 'Bad Request' });
  return resp.status(200).send(res);
};
