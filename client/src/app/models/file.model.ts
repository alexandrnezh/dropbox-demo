export class File {
  _id: string;
  name: string;
  path: string;
  size: number;
  isModified: boolean;
  isFolder: boolean;
  dateModified: Date;
  dateCreated: Date;
  type: string;
  extension: string;
  files: File[];
  parentFolder: File;
}
