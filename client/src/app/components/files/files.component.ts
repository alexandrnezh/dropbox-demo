import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { FileService } from "src/app/services/file.service";
import { File } from "src/app/models/file.model";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from "@angular/forms";
import {
  ActivatedRoute,
  Router,
  NavigationStart,
  NavigationEnd,
} from "@angular/router";
import { parseHostBindings } from "@angular/compiler";

@Component({
  selector: "app-files",
  templateUrl: "./files.component.html",
  styleUrls: ["./files.component.css"],
})
export class FilesComponent implements OnInit {
  @ViewChild("snackbar") snackbar: ElementRef;
  @ViewChild("snackbarFile") snackbarFileUploading: ElementRef;
  file;
  files: File[] = new Array();
  oldFiles: File[] = new Array();
  path = "/";
  private parentFolderId;
  public folderName: string = "";
  public fileForm: FormGroup;
  public folderForm: FormGroup;
  counter: number = 5;
  isUndidDeleting: boolean = false;

  constructor(
    private readonly fileService: FileService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.router.events.subscribe(async (val) => {
      if (val instanceof NavigationEnd) {
        this.getContentByPath(val.url);
      }
    });

    this.folderForm = formBuilder.group({
      name: ["", [Validators.required, Validators.maxLength(20)]],
    });
    this.fileForm = formBuilder.group({
      file: ["", [Validators.required]],
    });
  }

  handleFileInput(files: FileList) {
    this.file = files.item(0);
  }

  async goParentFolder() {
    const parentFolder = await this.fileService.get(this.parentFolderId);
    if (parentFolder.parentFolder) {
      this.router.navigateByUrl(parentFolder.parentFolder.path);
      await this.getContentByPath(parentFolder.parentFolder.path);
    } else {
      this.router.navigateByUrl("");
      await this.getContentByPath("");
    }
  }

  async uploadFile() {
    if (this.file) {
      this.snackbarFileUploading.nativeElement.className = "show snackbar";
      const res: any = await this.fileService.uploadFile(this.file, this.path);
      await this.fileService.createFile({
        name: res.name,
        path: res.path_display,
        isFolder: false,
        type: "File",
        extension: res.name.substr(res.name.lastIndexOf(".") + 1),
        parentFolder: this.parentFolderId,
        size: this.file.size,
      });

      await this.getContentByPath(this.path);
      this.snackbarFileUploading.nativeElement.className = this.snackbarFileUploading.nativeElement.className.replace(
        "show snackbar",
        "snackbar"
      );
    }
  }

  get name() {
    return this.folderForm.get("name");
  }

  get formFile() {
    return this.fileForm.get("file");
  }

  deleteFile(file: File) {
    this.snackbar.nativeElement.className = "show snackbar";
    this.oldFiles = this.files;
    this.files = this.files.filter((f) => f._id !== file._id);
    const interval = setInterval(async () => {
      this.counter--;
      if (this.isUndidDeleting) {
        this.files = this.oldFiles;
        this.snackbar.nativeElement.className = this.snackbar.nativeElement.className.replace(
          "show snackbar",
          "snackbar"
        );
      } else if (this.counter < 0) {
        this.snackbar.nativeElement.className = this.snackbar.nativeElement.className.replace(
          "show snackbar",
          "snackbar"
        );
        clearInterval(interval);
        this.counter = 5;
        if (file.parentFolder)
          this.router.navigateByUrl(file.parentFolder.path);
        else this.router.navigateByUrl("");
        await this.fileService.deleteFile(file._id);
        this.getContentByPath(this.router.url);
      }
    }, 1000);
  }

  async ngOnInit(): Promise<void> {}

  async getContentByPath(path: string) {
    const res = await this.fileService.getContentByPath(path);
    if (path === "/" || path === "") {
      this.path = "";
      this.files = res;
      this.parentFolderId = undefined;
    } else {
      this.path = path;
      this.files = res[0].files;
      this.parentFolderId = res[0]._id;
    }
  }

  async createFolder() {
    const createdPath = this.path + `/${this.folderName}`;
    await this.fileService.createFolder({
      name: this.folderName,
      path: createdPath,
      isFolder: true,
      type: "Folder",
      parentFolder: this.parentFolderId,
    });
    this.folderName = "";
    await this.getContentByPath(this.path);
  }
}
