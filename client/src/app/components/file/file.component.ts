import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { File } from "src/app/models/file.model";
import { FileService } from "src/app/services/file.service";
import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "app-file",
  templateUrl: "./file.component.html",
  styleUrls: ["./file.component.css"],
})
export class FileComponent implements OnInit {
  @Input() file: File;
  @Output() updateFilesEvent = new EventEmitter<string>();
  @Output() deleteFileEvent = new EventEmitter<File>();
  isHover = false;

  constructor(
    private readonly fileService: FileService,
    private readonly router: Router
  ) {}
  mouseHovering() {
    this.isHover = true;
  }
  mouseLeaving() {
    this.isHover = false;
  }

  async deleteFile() {
    this.deleteFileEvent.emit(this.file);
  }

  ngOnInit(): void {}
}
