import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { env } from "process";

@Injectable({
  providedIn: "root",
})
export class FileService {
  readonly apiUrl: string;
  readonly apiKey: string;

  constructor(private httpClient: HttpClient) {
    this.apiUrl = environment.api;
    this.apiKey = environment.api_key;
  }

  getAll(): Promise<any> {
    const header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + this.apiKey);
    return this.httpClient
      .get(this.apiUrl, {
        headers: header,
      })
      .toPromise();
  }

  get(id: string): Promise<any> {
    const header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + this.apiKey);
    return this.httpClient
      .get(this.apiUrl + `/${id}`, {
        headers: header,
      })
      .toPromise();
  }

  getContentByPath(path): Promise<any> {
    const header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + this.apiKey);
    return this.httpClient
      .get(this.apiUrl + `?path=${path}`, {
        headers: header,
      })
      .toPromise();
  }

  deleteFile(id): Promise<any> {
    const header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + this.apiKey);
    return this.httpClient
      .delete(this.apiUrl + `/${id}`, {
        headers: header,
      })
      .toPromise();
  }

  createFolder(createFolderDTO): Promise<any> {
    const header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + this.apiKey);
    return this.httpClient
      .post(this.apiUrl + "/createFolder", createFolderDTO, {
        headers: header,
      })
      .toPromise();
  }

  uploadFile(file: File, path: string) {
    const header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/octet-stream")
      .set(
        "Authorization",
        "Bearer PRHBiOErpRAAAAAAAAAAIfzwXKcVHX1IDlavSBnb82ddKfPn_rYEYyCtwYQgFi51"
      )
      .set(
        "Dropbox-API-Arg",
        JSON.stringify({
          path: path + `/${file.name}`,
          mode: "add",
          autorename: true,
          mute: false,
          strict_conflict: false,
        })
      );
    return this.httpClient
      .post("https://content.dropboxapi.com/2/files/upload", file, {
        headers: header,
      })
      .toPromise();
  }

  createFile(creatFileDTO) {
    const header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + this.apiKey);
    return this.httpClient
      .post(this.apiUrl + "/createFile", creatFileDTO, {
        headers: header,
      })
      .toPromise();
  }
}
